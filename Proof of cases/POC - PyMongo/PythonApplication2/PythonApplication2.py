import pymongo
from pymongo import mongo_client

print ("================ Navegando e manipulando o MongoDB ================")

#Ler o Banco da Berghem
try:
    client = pymongo.MongoClient('192.168.1.3', 27017)

except:
    print 'Nao foi possível conectar-se ao Banco. Tente novamente.'

db = client.Berghem

#Lista todas as colecoes existentes no banco
for colec in db.collection_names():
    print str(colec)

#Ler uma collection selecionada
#collection = str(raw_input("Entre com um cliente: "))
collection = 'Santander'
collection = db[collection]

#Mostrar itens na Collection pelo ID
print ("====== Projetos ======")
for item in collection.find():
    print (item['_id'])

#Capturando o projeto desejado pela ID
projeto = str(raw_input("Entre com o projeto: "))
#projeto = "Way"

#Atribuindo este projeto a uma variavel
#Aqui já temos a instancia do documento em si
projeto = collection.find_one({"_id": projeto})

#Exibicao das telas que estao dentro do documento "Appium"
print ('====== Scripts Appium dentro de projeto ======')
for item in projeto["Appium"]:
    print item

#No caso temos todos os atributos contidos no elemento selecionado, que no caso sao os objetos dos scripts do Appium
appium = str(raw_input("Entre com a primeira navegacao: "))
#appium = 'Login'

print ("===== Itens dentro da tela de " + str(appium) + " =====")
#Primeiro subdocumento percorrido [Login, Transferencia, AcessoPerfil, etc]
appium = projeto["Appium"][appium]
#Vamos ver
for item in appium:
    print item

#Mostrando os tipos de teste Existentes
tiposTeste = appium["TiposTeste"]

print ("===== Tipos de teste na tela =====")
for item in tiposTeste:
    print item

#Entrando nos tipos de testes existentes
teste = str(raw_input("Entre com o tipo de teste: "))
testes = tiposTeste[teste]

print " =========== Testes Salvos =========== "

for item in testes:
    print item

print " =========== Objetos dentro do teste =========== "
print testes


"""     Insercao de elementos no documento
    Para que a POC seja valida, realizar a insercao dos elementos:
    - Projeto
    - Script Appium
    - Tipo de Teste
    - Ponto (Com Dados e RespostaEsperada)"""

## ===================== Insercao de Projeto =====================

#newProj = str(raw_input("Entre com o nome do novo Projeto: "))

#dic = {"_id" : newProj, "Appium" : ""}

#try:
#    collection.insert(dic)
#except pymongo.errors.DuplicateKeyError:
#    print "Falha na insercao do Projeto, " + newProj + " ja existe para este cliente"
#except:
#    print "Falha na insercao do Projeto, tente novamente"

## ===================== Insercao de Script Appium =====================
##Seleciona o projeto
#projeto = str(raw_input("Em qual projeto deseja inserir o Script? "))

##Atribui o objeto do documento a variavel de acordo com o selecionado
#projeto = collection.find_one({"_id": projeto})

##Inserir por partes?
##Define nome do Script
#appium = str(raw_input("Qual o nome do Script? "))

##Define o codigo
#codigo = str(raw_input("Entre com o codigo: "))
#collection.update({"_id" : projeto["_id"], }, {"$set" : {
#                                                            "Appium"+"."+appium : { "Codigo" : codigo, "TiposTeste" : {}}
#                                                        }
#                                               }, True)

## ===================== Insercao de Tipo de Teste =====================

##Seleciona o projeto
#projeto = str(raw_input("Entre com o projeto a ser selecionado: "))

##Atribui o objeto do documento a variavel de acordo com o selecionado
#projeto = collection.find_one({"_id": projeto})

##Seleciona o Script
#appium = str(raw_input("A qual script o Tipo de Teste pertence? "))

##Define o tipo de teste
#tipoTeste = str(raw_input("Entre com o Tipo do Teste: "))

#collection.update({"_id" : projeto["_id"]}, {"$set" : {
#                                                    "Appium"+"."+appium+"."+"TiposTeste"+"."+tipoTeste : {}
#                                                }
#                                             }, True)

### ===================== Insercao de Ponto =====================
##Seleciona o projeto
##projeto = str(raw_input("Entre com o projeto a ser selecionado: "))

##Atribui o objeto do documento a variavel de acordo com o selecionado
##projeto = collection.find_one({"_id": projeto})

##Seleciona o Script
##appium = str(raw_input("A qual script o Tipo de Teste pertence? "))

##Seleciona o tipo de teste
##tipoTeste = str(raw_input("Entre com o Tipo do Teste: "))

##Define o ponto

##ponto = str(raw_input("Entre com o nome do Teste/Ponto: "))

##mime = str(raw_input("Entre com o tipo de MIME do Teste/Ponto: "))
##Dados = {"url" : "www.youtube.com", 
##                 "TestesReq" : "",
##                 "TestesRes" : "",
##                 "TestesHeadReq" : "",
##                 "TestesHeadRes" : ""}

##collection.update({"_id" : projeto["_id"]}, {"$set" : {
##                                                    "Appium"+"."+appium+"."+"TiposTeste"+"."+tipoTeste+"."+ponto : {"tipoMIME" : mime, "Dados":Dados,"RespostaEsperada":{}}
##                                                }
##                                             }, True)
