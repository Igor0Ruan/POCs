var lstResponse = [
    {
        "code": "50",
        "period": "ÚLTIMO ANO",
        "description": null,
        "label": null,
        "value": 133,
        "valueDecimal": null,
        "toolText": null,
        "color": null,
        "defaultColor": null,
        "tipo": null,
        "chartType": null,
        "valueFormated": "133",
        "headerTitle": null,
        "rowTitle": "ÚLTIMO ANO"
    },
    {
        "code": null,
        "period": "JAN",
        "description": null,
        "label": null,
        "value": 93,
        "valueDecimal": null,
        "toolText": null,
        "color": null,
        "defaultColor": null,
        "tipo": null,
        "chartType": null,
        "valueFormated": "93",
        "headerTitle": null,
        "rowTitle": "JAN"
    },
    {
        "code": null,
        "period": "FEV",
        "description": null,
        "label": null,
        "value": 112,
        "valueDecimal": null,
        "toolText": null,
        "color": null,
        "defaultColor": null,
        "tipo": null,
        "chartType": null,
        "valueFormated": "112",
        "headerTitle": null,
        "rowTitle": "FEV"
    },
    {
        "code": null,
        "period": "MAR",
        "description": null,
        "label": null,
        "value": 101,
        "valueDecimal": null,
        "toolText": null,
        "color": null,
        "defaultColor": null,
        "tipo": null,
        "chartType": null,
        "valueFormated": "101",
        "headerTitle": null,
        "rowTitle": "MAR"
    },
    {
        "code": null,
        "period": "ABR",
        "description": null,
        "label": null,
        "value": 104,
        "valueDecimal": null,
        "toolText": null,
        "color": null,
        "defaultColor": null,
        "tipo": null,
        "chartType": null,
        "valueFormated": "104",
        "headerTitle": null,
        "rowTitle": "ABR"
    },
    {
        "code": null,
        "period": "MAI",
        "description": null,
        "label": null,
        "value": 87,
        "valueDecimal": null,
        "toolText": null,
        "color": null,
        "defaultColor": null,
        "tipo": null,
        "chartType": null,
        "valueFormated": "87",
        "headerTitle": null,
        "rowTitle": "MAI"
    },
    {
        "code": null,
        "period": "JUN",
        "description": null,
        "label": null,
        "value": 0,
        "valueDecimal": null,
        "toolText": null,
        "color": null,
        "defaultColor": null,
        "tipo": null,
        "chartType": null,
        "valueFormated": "0",
        "headerTitle": null,
        "rowTitle": "JUN"
    },
    {
        "code": null,
        "period": "JUL",
        "description": null,
        "label": null,
        "value": 0,
        "valueDecimal": null,
        "toolText": null,
        "color": null,
        "defaultColor": null,
        "tipo": null,
        "chartType": null,
        "valueFormated": "0",
        "headerTitle": null,
        "rowTitle": "JUL"
    },
    {
        "code": null,
        "period": "AGO",
        "description": null,
        "label": null,
        "value": 0,
        "valueDecimal": null,
        "toolText": null,
        "color": null,
        "defaultColor": null,
        "tipo": null,
        "chartType": null,
        "valueFormated": "0",
        "headerTitle": null,
        "rowTitle": "AGO"
    },
    {
        "code": null,
        "period": "SET",
        "description": null,
        "label": null,
        "value": 0,
        "valueDecimal": null,
        "toolText": null,
        "color": null,
        "defaultColor": null,
        "tipo": null,
        "chartType": null,
        "valueFormated": "0",
        "headerTitle": null,
        "rowTitle": "SET"
    }
];


function buidChartData(data) {
    categoriesValues = [];
    monthValues = [];
    monthGoals = [];
    data.forEach(element => {
        categoriesValues.push({label: element.rowTitle});
        monthValues.push({value: element.value});
        monthGoals.push({value: 'valor da meta mensal'});
    });

    data = {
        chart: {
            yaxisname: "Valores por mês",
            syaxisname: "Meta do mês",
            labeldisplay: "rotate",
            scrollheight: "10",
            drawcrossline: "1",
            theme: "fusion"
        },
        categories: [
            {
                category: categoriesValues
            }
        ],
        dataset: [
            {
                seriesname: "Valores por mês",
                data: monthValues
            },
            {
                seriesname: "Meta do Mês",
                parentyaxis: "S",
                renderas: "line",
                showvalues: "0",
                data: monthGoals
            },   
        ]
    }

    return data;
}

console.log(buidChartData(lstResponse));