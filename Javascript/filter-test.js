var objList = [
    {
        nome: 'Igor',
        idade: '22',
        sexo: 'M',
        pais: 'Brasil'
    },
    {
        nome: 'Lievi',
        idade: '23',
        sexo: 'M',
        pais: 'Brasil'
    },
    {
        nome: 'Luis',
        idade: '28',
        sexo: 'M',
        pais: 'Canadá'
    },
    {
        nome: 'Carolina',
        idade: '24',
        sexo: 'F',
        pais: 'França'
    },
    {
        nome: 'Dayane',
        idade: '18',
        sexo: 'F',
        pais: 'Brasil'
    },
]

var europe = ['França', 'Itália', 'Alemanha']
var america = ['Brasil', 'Canadá', 'Venezuela']

var filtrarPorContinente = function (pessoas, continente) {
    return pessoas.filter(function (pessoa) {
        return continente.some(function (pais) {
            return pessoa.pais === pais;
        });
    });
}

console.log(JSON.stringify(filtrarPorContinente(objList, europe), null, 2));