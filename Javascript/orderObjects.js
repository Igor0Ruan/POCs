var captions = [
    {
        text: "A",
        startTime: 800
    },
    {
        text: "B",
        startTime: 1800
    },
    {
        text: "C",
        startTime: 500
    },
    {
        text: "D",
        startTime: 0
    },
    {
        text: "E",
        startTime: 3000
    }
];

captions.sort((a, b) => {
    return a.startTime - b.startTime;
})

captions.map((caption) => {
    caption.startTime = caption.startTime.toString();

    return caption;
})

console.log(captions, null, 1);