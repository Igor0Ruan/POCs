function orderHierarchy(item) {
    if (item.hasOwnProperty('parent')) {
        var tree = {
            id: item.parent.id,
            name: item.parent.name,
            childs: [
                {
                    id: item.id,
                    name: item.name
                }
            ]
        };

        if (item.hasOwnProperty('childs'))
            tree.childs[0].childs = item.childs;

        if (item.parent.hasOwnProperty('parent')) {
            tree.parent = item.parent.parent;
            tree = orderHierarchy(tree);
        }

        return tree;

    } else {
        return item;
    }
}

function mergeArrays(nestedItems) {
    var merged = [];
    var ids = [];

    nestedItems.forEach(function (n_item) {
        if ((ids.indexOf(n_item.id) >= 0) && (n_item.hasOwnProperty('childs'))) {
            merged.forEach(function (m_item) {
                if (n_item.id === m_item.id) {
                    if (!m_item.hasOwnProperty('childs')) {
                        m_item.childs = [];
                    }

                    n_item.childs.forEach(function (child) {
                        m_item.childs.push(child);
                    });
                }
            });
        } else if (!ids.indexOf(n_item.id) >= 0) {
            merged.push(n_item);
            ids.push(n_item.id);
        }
    });

    merged.forEach(function (value) {
        if (value.hasOwnProperty('childs')) {
            value.childs = mergeArrays(value.childs);
        }
    });

    return merged;
}

var categories = [
    {
        "parent": {
            "id": 1,
            "name": "My sites"
        },
        "id": 5,
        "name": "Corporate"
    },
    {
        "parent": {
            "id": 1,
            "name": "My sites"
        },
        "id": 2,
        "name": "e-Commerce"
    },
    {
        "parent": {
            "id": 1,
            "name": "My sites"
        },
        "id": 3,
        "name": "Institutional"
    },
    {
        "parent": {
            "parent": {
                "id": 1,
                "name": "My sites"
            },
            "id": 2,
            "name": "e-Commerce"
        },
        "id": 9,
        "name": "Mens & Women wear"
    },
    {
        "id": 1,
        "name": "My sites"
    },
    {
        "parent": {
            "parent": {
                "id": 1,
                "name": "My sites"
            },
            "id": 5,
            "name": "Corporate"
        },
        "id": 6,
        "name": "Salesforce"
    },
    {
        "parent": {
            "parent": {
                "id": 1,
                "name": "My sites"
            },
            "id": 5,
            "name": "Corporate"
        },
        "id": 7,
        "name": "SAP"
    },
    {
        "id": 4,
        "name": "Third-parties"
    },
    {
        "parent": {
            "parent": {
                "id": 1,
                "name": "My sites"
            },
            "id": 2,
            "name": "e-Commerce"
        },
        "id": 8,
        "name": "Toyshop"
    }
];

var nestedItems = orderHierarchy(categories);

var result = mergeArrays(nestedItems);

console.log(JSON.stringify(result, null, 1));