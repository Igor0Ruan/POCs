<?php

$timeZoneUTC = new DateTimeZone('UTC');
$mask = 'Y-m-d H:i';

$datetime = new DateTime('now');
$datetime->setTimezone($timeZoneUTC);
$datetime->format($mask);

echo $datetime;
 