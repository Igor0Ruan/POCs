import { Component, OnInit, AfterViewInit } from '@angular/core';
import * as L from 'leaflet';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements AfterViewInit {
  private map = null;
  private divIcon: any;
  private markers: any[];
  private selectedMarker: any;

  constructor() { }

  ngAfterViewInit(): void {
    this.createMap();
    this.updateMarkers();
  }

  private createMap(): void {
    this.map = this.buildMap([0, 0]);

    this.divIcon = L.DivIcon.extend({
      iconSize: new L.Point(20, 20),
      className: 'my-div-icon',
      createIcon() {
        const bgcolor = this.options.bgcolor || 'rgb(255, 255, 255)';
        const color = this.options.color || 'rgb(0, 0, 0)';
        const div = document.createElement('div');
        const divBorder = document.createElement('div');
        const numdiv = document.createElement('div');
        numdiv.setAttribute('class', 'numberCircle');
        numdiv.setAttribute('style', 'background-color: ' + bgcolor + ';color: ' + color);
        numdiv.innerHTML = this.options.number || '0';
        divBorder.setAttribute('class', 'div-map-icon-active');
        divBorder.appendChild(numdiv);
        div.appendChild(divBorder);
        return div;
      }
    });
  }

  buildMap(coord: L.LatLngExpression) {
    let map;
    this.markers = L.featureGroup();

    // CRIANDO MAPA CINZA
    const mbAttr = '';
    const mbUrl = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';

    const grayscale = L.tileLayer(mbUrl, { id: 'mapbox.light', attribution: mbAttr });

    map = L.map('map', {
      zoomControl: false,
      zoom: 2,
      minZoom: 2,
      attributionControl: false,
      center: coord,
      layers: [grayscale, this.markers]
    });
    L.control.zoom({
      position: 'bottomleft'
    }).addTo(map);

    return map;
  }

  getMarkerOptions() {

    const options = {
      bgcolor: 'rgb(116,190,116)',
      color: 'rgb(255,255,255)',
      eventCount: 0
    };

    const eventList = [{
      backgroundColor: '#ED2E38',
      fontColor: '#FFFFFF'
    }];

    options.eventCount = eventList.length;
    options.bgcolor = eventList[0].backgroundColor;
    options.color = eventList[0].fontColor;

    return options;
  }

  updateMarkers() {
    const latitude = 39.0438;
    const longitude = -77.4874;
    const city = 'Washington';

    const markerOptions = this.getMarkerOptions();

    this.divIcon.number = markerOptions.eventCount;
    this.divIcon.color = markerOptions.color;
    this.divIcon.bgcolor = markerOptions.bgcolor;

    const marker = L.marker(
      [latitude, longitude],
      {
        icon: new this.divIcon
      }
    ).bindTooltip(city, {
      className: 'map-tooltip',
      direction: 'top'
    })
    .openTooltip()
    .on('click', (e) => {
      console.log('Target: ', e.target);
    });

    marker.addTo(this.markers);

  }

}
