import pymongo

client = pymongo.MongoClient('mongodb://localhost:27017')

collection = client['BerghemData']['Results']
collection2 = client['BerghemAutomation']['com.teste.testeigor.br']

doc = {
    'horario': "2018-06-08 14:42:29.799Z",
    'resultadoTeste': False,
    'requests': [{
        'id': 1,
        'url': '/endpoint/teste',
        'raw': {
            'requisicao': {
                'header': ['POST', 'HTTP'],
                'body': 'Enviando valores'
            },
            'resposta': {
                'header': ['500 Internal Server Error', 'HTTP', 'Proxy off'],
                'body': 'Falha interna do servidor, por favor tente novamente'
            }
        },
        'parsed': {
            'requisicao': {
                'header': [{
                    'chave': 'Method',
                    'valor': 'POST'
                },
                    {
                    'chave': 'Protocol',
                    'valor': 'HTTP'
                }],
                'body': [{
                    'chave': 'Chamada',
                    'valor': 'Enviando valores'
                }],
                'tipoMime': 'text'
            },
            'resposta': {
                'header': [{
                    'chave': 'Status',
                    'valor': '500'
                },
                    {
                    'chave': 'Protocol',
                    'valor': 'HTTP'
                },
                    {
                    'chave': 'Proxy Status',
                    'valor': 'Proxy off'
                }],
                'body': [{
                    'chave': 'Mensagem',
                    'valor': 'Falha interna do servidor, por favor tente novamente'
                }],
                'tipoMime': 'text'
            }
        },
        'resultadoEsperado': {
            'request': {
                'header': {},
                'body': {}
            },
            'response': {
                'header': {
                    'valorEsperado': '200',
                    'igual': False,
                    'tagModificada': 'Status',
                    'valorAtual': '500'
                },
                'body': {}
            }
        },
        'testesRealizados': {
            'request': {
                'original': {
                    'header': ['POST', 'HTTPS'],
                    'body': 'Enviando valores'
                },
                'header': [{
                    'valorOriginal': 'HTTPS',
                    'tipoTeste': 'MudarTokenParaValorSetado',
                    'tag': 'Protocol',
                    'valorModificado': 'HTTP'
                }],
                'body': []
            },
            'response': {
                'original': {
                    'header': ['500', 'Internal Server Error', 'Proxy off'],
                    'body': 'Falha interna do servidor, por favor tente novamente'
                },
                'header': [],
                'body': []
            },
        }
    },
        {
        'id': 2,
        'url': '/endpoint2/teste2',
        'raw': {
            'requisicao': {
                'header': ['POST', 'HTTP'],
                'body': 'Enviando valores'
            },
            'resposta':{
                'header': ['500 Internal Server Error', 'HTTP', 'Proxy off'],
                'body': 'Falha interna do servidor, por favor tente novamente'
            }
        },
        'parsed': {
            'requisicao': {
                'header': [{
                    'chave': 'Method',
                    'valor': 'POST'
                },
                    {
                    'chave': 'Protocol',
                    'valor': 'HTTP'
                }],
                'body': [{
                    'chave': 'Chamada',
                    'valor': 'Enviando valores'
                }],
                'tipoMime': 'text'
            },
            'resposta': {
                'header': [{
                    'chave': 'Status',
                    'valor': '500'
                },
                    {
                    'chave': 'Protocol',
                    'valor': 'HTTP'
                },
                    {
                    'chave': 'Proxy Status',
                    'valor': 'Proxy off'
                }],
                'body': [{
                    'chave': 'Mensagem',
                    'valor': 'Falha interna do servidor, por favor tente novamente'
                }],
                'tipoMime': 'text'
            }
        },
        'resultadoEsperado': {},
        'testesRealizados': {}
    }],
    'erros': {
        'script': '',
        'burp': ''
    }
}
count = 1
while count < 5000:
    print "Inserindo pela " + str(count) + "ª vez"
    _id = collection.insert(doc)

    collection2.update(
        {'_id': 'Script de Teste'}, {
            '$push': {
                'TiposTeste.ModificarRequestResponse.Teste123.Resultado': _id
            }
        }, True)
    print (type(_id), _id)
    print (type(_id.__str__()), _id.__str__())
    doc.pop('_id', None)
    count += 1
