import socket
import time

client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_socket.connect(("192.168.1.82", 8080))

client_socket.send("start_hook\n")
while True:
    inp = client_socket.recv(2048)
    if inp == "Done!":
        print inp
        break

# #send disconnect message                                                                                                                           
# dmsg = "disconnect"
# print "Disconnecting"
# client_socket.send(dmsg)

client_socket.close()