import json

categories = [
    {
        "parent": {
            "id": 1,
            "name": "My sites"
        },
        "id": 5,
        "name": "Corporate"
    },
    {
        "parent": {
            "id": 1,
            "name": "My sites"
        },
        "id": 2,
        "name": "e-Commerce"
    },
    {
        "parent": {
            "id": 1,
            "name": "My sites"
        },
        "id": 3,
        "name": "Institutional"
    },
    {
        "parent": {
            "parent": {
                "id": 1,
                "name": "My sites"
            },
            "id": 2,
            "name": "e-Commerce"
        },
        "id": 9,
        "name": "Mens & Women wear"
    },
    {
        "id": 1,
        "name": "My sites"
    },
    {
        "parent": {
            "parent": {
                "id": 1,
                "name": "My sites"
            },
            "id": 5,
            "name": "Corporate"
        },
        "id": 6,
        "name": "Salesforce"
    },
    {
        "parent": {
            "parent": {
                "id": 1,
                "name": "My sites"
            },
            "id": 5,
            "name": "Corporate"
        },
        "id": 7,
        "name": "SAP"
    },
    {
        "id": 4,
        "name": "Third-parties"
    },
    {
        "parent": {
            "parent": {
                "id": 1,
                "name": "My sites"
            },
            "id": 2,
            "name": "e-Commerce"
        },
        "id": 8,
        "name": "Toyshop"
    }
]

test_item = {
    "parent": {
        "parent": {
            "id": 1,
            "name": "My sites"
        },
        "id": 5,
        "name": "Corporate"
    },
    "id": 7,
    "name": "SAP"
}

nested_items = [
    {
        "id": 1,
        "name": "My sites",
        "childs": [
            {
                "id": 5,
                "name": "Corporate"
            }
        ]
    },
    {
        "id": 1,
        "name": "My sites",
        "childs": [
            {
                "id": 2,
                "name": "e-Commerce"
            }
        ]
    },
    {
        "id": 1,
        "name": "My sites",
        "childs": [
            {
                "id": 3,
                "name": "Institutional"
            }
        ]
    },
    {
        "id": 1,
        "name": "My sites",
        "childs": [
            {
                "id": 2,
                "name": "e-Commerce",
                "childs": [
                    {
                        "id": 9,
                        "name": "Mens & Women wear"
                    }
                ]
            }
        ]
    },
    {
        "id": 1,
        "name": "My sites"
    },
    {
        "id": 1,
        "name": "My sites",
        "childs": [
            {
                "id": 5,
                "name": "Corporate",
                "childs": [
                    {
                        "id": 6,
                        "name": "Salesforce"
                    }
                ]
            }
        ]
    },
    {
        "id": 1,
        "name": "My sites",
        "childs": [
            {
                "id": 5,
                "name": "Corporate",
                "childs": [
                    {
                        "id": 7,
                        "name": "SAP"
                    }
                ]
            }
        ]
    },
    {
        "id": 4,
        "name": "Third-parties"
    },
    {
        "id": 1,
        "name": "My sites",
        "childs": [
            {
                "id": 2,
                "name": "e-Commerce",
                "childs": [
                    {
                        "id": 8,
                        "name": "Toyshop"
                    }
                ]
            }
        ]
    }
]


def orderHierarchy(item):
    if 'parent' in item:
        tree = {
            'id': item['parent']['id'],
            'name': item['parent']['name'],
            'childs': [
                {
                    'id': item['id'],
                    'name': item['name']
                }
            ]
        }

        if 'childs' in item:
            tree['childs'][0]['childs'] = item['childs']

        if 'parent' in item['parent']:
            tree['parent'] = item['parent']['parent']
            tree = orderHierarchy(tree)
        return tree
    else:
        return item


def mergeArrays(nested_items):
    merged = []
    ids = []

    for item in nested_items:
        if (item['id'] in ids) and ('childs' in item):
            for m_item in merged:
                if item['id'] == m_item['id']:
                    if 'childs' not in m_item:
                        m_item['childs'] = []
                    for child in item['childs']:
                        m_item['childs'].append(child)
        elif (item['id'] not in ids):
            merged.append(item)
            ids.append(item['id'])

    for item in merged:
        if 'childs' in item:
            item['childs'] = mergeArrays(item['childs'])

    return merged


def setTreeCategories(categories):
    nested_items = []
    for categorie in categories:
        nested_items.append(orderHierarchy(categorie))

    grouped_categories = mergeArrays(nested_items)

    print(json.dumps(grouped_categories, indent=4))


setTreeCategories(categories)
