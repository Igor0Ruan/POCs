import pymongo

db = pymongo.MongoClient('mongodb://10.0.200.188:27017')

db = db['BerghemAutomation']
for item in db.collection_names():
    if item != 'com.android.insecurebankv2.apk' and item != 'system.indexes':
        db.drop_collection(item)
