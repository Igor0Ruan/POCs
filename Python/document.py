class Document:
    doc = {
        'horario': "2018-06-08 14:42:29.799Z",
        'resultadoTeste': False,
        'requests': [{
            'id': 1,
            'url': '/endpoint/teste',
            'raw': {
                'requisicao': {
                    'header': ['POST', 'HTTP'],
                    'body': 'Enviando valores'
                },
                'resposta': {
                    'header': ['500 Internal Server Error', 'HTTP', 'Proxy off'],
                    'body': 'Falha interna do servidor, por favor tente novamente'
                }
            },
            'parsed': {
                'requisicao': {
                    'header': [{
                        'chave': 'Method',
                        'valor': 'POST'
                    },
                        {
                        'chave': 'Protocol',
                        'valor': 'HTTP'
                    }],
                    'body': [{
                        'chave': 'Chamada',
                        'valor': 'Enviando valores'
                    }],
                    'tipoMime': 'text'
                },
                'resposta': {
                    'header': [{
                        'chave': 'Status',
                        'valor': '500'
                    },
                        {
                        'chave': 'Protocol',
                        'valor': 'HTTP'
                    },
                        {
                        'chave': 'Proxy Status',
                        'valor': 'Proxy off'
                    }],
                    'body': [{
                        'chave': 'Mensagem',
                        'valor': 'Falha interna do servidor, por favor tente novamente'
                    }],
                    'tipoMime': 'text'
                }
            },
            'resultadoEsperado': {
                'request': {
                    'header': {},
                    'body': {}
                },
                'response': {
                    'header': {
                        'valorEsperado': '200',
                        'igual': False,
                        'tagModificada': 'Status',
                        'valorAtual': '500'
                    },
                    'body': {}
                }
            },
            'testesRealizados': {
                'request': {
                    'original': {
                        'header': ['POST', 'HTTPS'],
                        'body': 'Enviando valores'
                    },
                    'header': [{
                        'valorOriginal': 'HTTPS',
                        'url': '/endpoint/teste',
                        'tipoTeste': 'MudarTokenParaValorSetado',
                        'tag': 'Protocol',
                        'valorModificado': 'HTTP'
                    }],
                    'body': []
                },
                'response': {
                    'original': {
                        'header': ['500', 'Internal Server Error', 'Proxy off'],
                        'body': 'Falha interna do servidor, por favor tente novamente'
                    },
                    'header': [],
                    'body': []
                },
            }
        },
            {
            'id': 2,
            'url': '/endpoint2/teste2',
            'raw': {
                'requisicao': {
                    'header': ['POST', 'HTTP'],
                    'body': 'Enviando valores'
                },
                'resposta':{
                    'header': ['500 Internal Server Error', 'HTTP', 'Proxy off'],
                    'body': 'Falha interna do servidor, por favor tente novamente'
                }
            },
            'parsed': {
                'requisicao': {
                    'header': [{
                        'chave': 'Method',
                        'valor': 'POST'
                    },
                        {
                        'chave': 'Protocol',
                        'valor': 'HTTP'
                    }],
                    'body': [{
                        'chave': 'Chamada',
                        'valor': 'Enviando valores'
                    }],
                    'tipoMime': 'text'
                },
                'resposta': {
                    'header': [{
                        'chave': 'Status',
                        'valor': '500'
                    },
                        {
                        'chave': 'Protocol',
                        'valor': 'HTTP'
                    },
                        {
                        'chave': 'Proxy Status',
                        'valor': 'Proxy off'
                    }],
                    'body': [{
                        'chave': 'Mensagem',
                        'valor': 'Falha interna do servidor, por favor tente novamente'
                    }],
                    'tipoMime': 'text'
                }
            },
            'resultadoEsperado': {},
            'testesRealizados': {}
        }],
        'erros': {
            'script': '',
            'burp': ''
        }
    }

    def getDocument(self):
        return self.doc
