import pymongo
import datetime
import json
import codecs
import datetime
from bson import json_util, ObjectId

print ("=============== Exportação de resultados de testes ===============")
db = pymongo.MongoClient('mongodb://192.168.1.115:27017')


def myconverter(o):
    if isinstance(o, datetime.datetime):
        return o.__str__()


collection = db['BerghemData']['created_tests']
count = 0
for test in collection.find():
    id_result = test['resultados'][len(test['resultados']) - 1]
    collResult = db['BerghemData']['Results']
    docResult = json_util.dumps(collResult.find({'_id': id_result}))
    print(type(docResult))

    json_test = json.dumps(docResult, default=myconverter)
    print(type(json_test))

    json_test = json.loads(json_test)
    print(type(json_test))

    json_test = json_util.loads(json_test)
    print(type(json_test))

    json_test[0]['horario'] = json_test[0]['horario'].__str__()
    json_test[0]['_id'] = json_test[0]['_id'].__str__()

    json_test = json_test[0]

    with codecs.open('results/' + test['teste']['nomeTeste'] + '.json', "w", encoding='utf-8') as outfile:
        json.dump(json_test, outfile, ensure_ascii=False)

    # docResult = json_util.dumps(docResult)
    # print(docResult)

    # with open('results/' + test['teste']['nomeTeste'] + '.json', 'w') as outfile:
    #     json.dump(docResult, outfile)
