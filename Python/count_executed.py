import pymongo
import datetime

db = pymongo.MongoClient('mongodb://192.168.1.115:27017')

collection = db['BerghemData']['created_tests']

print "====================================== Contagem de testes Executados ======================================"
count = 0
for test in collection.find():
    if test['teste']['executando'] == False:
        count += 1
    else:
        break
print "Total de testes executados: " + str(count)
print "Horario da contagem: " + str(datetime.datetime.now())
