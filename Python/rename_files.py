import os

flags_path = "/home/igor0ruan/Projetos/NAVA/siteanalysis/frontend-ng/src/assets/images/flags/"

for file in os.listdir(flags_path):
  old_file = flags_path + file
  new_file = file.split('.svg')
  new_file = flags_path + "flag_" + new_file[0].upper() + ".svg"
  os.rename(old_file, new_file)
  