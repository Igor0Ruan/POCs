import pymongo
import datetime
import json
import codecs
from bson import json_util

print ("=============== Exportação de resultados de testes ===============")
db = pymongo.MongoClient('mongodb://192.168.1.115:27017')

collection = db['BerghemData']['Functionality']

test = json_util.dumps(collection.find(
    {'_id': 'Remoção e manipulação de TAGs ou parâmetros'}))

json_test = json.dumps(test)
print ("Tipo do teste >>>>>>> ", type(json_test))
print (json_test)


json_test = json.loads(json_test)
print ("Tipo do teste >>>>>>> ", type(json_test))
print (json_test)

json_test = json_util.loads(json_test)
print ("Tipo do teste >>>>>>> ", type(json_test))
print (json_test)


with codecs.open('arquivos/teste.json', "w", encoding='utf-8') as outfile:
    json.dump(json_test, outfile, ensure_ascii=False)

    # docResult = json_util.dumps(docResult)
    # print(docResult)

    # with open('results/' + test['teste']['nomeTeste'] + '.json', 'w') as outfile:
    #     json.dump(docResult, outfile)
