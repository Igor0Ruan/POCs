import pymongo
from bson.objectid import ObjectId
client = pymongo.MongoClient('mongodb://192.168.1.115:27017')
collection = client['BerghemData']['created_tests']

contractsNumbers = ['000000001', '000000002',
                    '000000003', '000000004', '000000005']

products = ['03530', '0006', '03665']

accounts = [
    {
        'account': '000130001341',  # valido
        'branch': '1417',
        'bank': '0000'
    },
    {
        'account': '000130311316',  # valido
        'branch': '0001',
        'bank': '0000'
    },
    {
        'account': '000130323630',  # valido
        'branch': '0001',
        'bank': '0000'
    },
    {
        'account': '000130015134',  # valido
        'branch': '0183',
        'bank': '0000'
    },
    {
        'account': '000130021966',  # valido
        'branch': '3689',
        'bank': '0000'
    },
    {
        'account': '000130001343',  # Brute force
        'branch': '0002',
        'bank': '0000'
    },
    {
        'account': '000130002682',  # Brute force
        'branch': '0001',
        'bank': '0000'
    },
    {
        'account': '000130002645',  # Brute force
        'branch': '0002',
        'bank': '0000'
    },
    {
        'account': '000130002849',  # Brute force
        'branch': '0002',
        'bank': '0000'
    },
    {
        'account': '000130007384',  # Brute force
        'branch': '0002',
        'bank': '0000'
    },
    {
        'account': '000130007940',  # Brute force
        'branch': '0002',
        'bank': '0000'
    },
    {
        'account': '000130001734',  # Brute force
        'branch': '0001',
        'bank': '0000'
    },
    {
        'account': '000130007502',  # Brute force
        'branch': '0001',
        'bank': '0000'
    },
    {
        'account': '000130007503',  # Brute force
        'branch': '1417',
        'bank': '0000'
    },
    {
        'account': '000130006139',  # Brute force
        'branch': '1417',
        'bank': '0000'
    }
]

doc = {
    "script": {
        "aplicativo": "com.santander.app.apk",
        "nomeScript": "Consulta de Fundos",
        "input": "Primeira",
        "serialNo": "32aab2540704",
        "tentativas": 1
    },
    "teste": {
        "nomeTeste": "Ponto 124 - CN3",
        "tipoTeste": "ModificarRequestResponse",
        "crypto": True,
        "urls": [
            {
                "enderecoUrl": "https://esbapihi.santander.com.br:443/individual-investment/v1/customer-portfolio/investment-detail",
                "resultadoEsperado": False,
                "requisicao": {
                    "tipoMime": "JSON",
                    "header": [],
                    "body": [
                        {
                            "crypto": True,
                            "tag": "branch##_account",
                            "dado": "1417",
                            "tipoTeste": "MudarTokenParaValorSetado"
                        },
                        {
                            "crypto": True,
                            "tag": "account##_account",
                            "dado": "000130001341",
                            "tipoTeste": "MudarTokenParaValorSetado"
                        },
                        {
                            "crypto": True,
                            "tag": "bank##_account",
                            "dado": "0000",
                            "tipoTeste": "MudarTokenParaValorSetado"
                        },
                        {
                            "crypto": True,
                            "tag": "contractnumber##",
                            "dado": "000000003",
                            "tipoTeste": "MudarTokenParaValorSetado"
                        },
                        {
                            "crypto": False,
                            "tag": "subproduct##",
                            "dado": "03530",
                            "tipoTeste": "MudarTokenParaValorSetado"
                        }
                    ]
                },
                "resposta": {
                    "tipoMime": "JSON",
                    "header": [],
                    "body": []
                }
            }
        ],
        "executando": False,
        "vulnerabilidade": "",
        "funcionalidade": ""
    }
}

entrada = 'inicio'

# while entrada != 'fim':
#     entrada = raw_input("Mais ou ta bom? ")
#     if entrada != 'fim':
#         count = 0
for data in accounts:
    for CN in contractsNumbers:
        for product in products:
            doc['teste']['urls'][0]['requisicao']['body'][0]['dado'] = data['branch']
            doc['teste']['urls'][0]['requisicao']['body'][1]['dado'] = data['account']
            doc['teste']['urls'][0]['requisicao']['body'][2]['dado'] = data['bank']
            doc['teste']['urls'][0]['requisicao']['body'][3]['dado'] = CN
            doc['teste']['urls'][0]['requisicao']['body'][4]['dado'] = product

            print "Documento preparado, inserindo no banco..."

            try:
                doc['teste']['nomeTeste'] = "Ponto 124 - Acc " + data['account'][4:] + \
                    " branch " + data['branch'] + " CN " + \
                    CN[-1:] + " Prod " + product
                doc['_id'] = str(ObjectId())
                collection.insert(doc)
                doc.pop('_id', None)
                print 'Documento inserido!'
                print 'Contract Number: ' + CN
                print 'Product: ' + product
            except Exception as e:
                print ("Opa, deu erro")
                print str(e)
