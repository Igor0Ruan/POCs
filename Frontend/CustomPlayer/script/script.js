var video = document.querySelector(".video");
var accent = document.querySelector(".red-accent");
var btn = document.querySelector("#play-pause");

function togglePlayPause() {
    if(video.paused) {
        btn.className = "pause";
        video.play();
    } else {
        btn.className = "play";
        video.pause();
    }
}

btn.onclick = function () {
    togglePlayPause();
}

video.onclick = function () {
    togglePlayPause();
}

video.addEventListener("timeupdate", () => {
    var accentPos = video.currentTime / video.duration;
    accent.style.transition = "width .7s";
    accent.style.width = accentPos * 100 + "%";

    if(video.ended) {
        btn.className = "play";
        accent.style.width = "0%";
        accent.style.transition = "none";
    }
})

btn.className = video.paused ? "play" : "pause";